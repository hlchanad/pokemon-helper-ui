import { Component, OnInit } from '@angular/core';
import { CounterService } from '../shared/counter.service';
import { Counter } from '../data/counter.interface';

@Component({
  selector: 'app-attack-optimizer',
  templateUrl: './attack-optimizer.component.html',
  styleUrls: ['./attack-optimizer.component.css']
})
export class AttackOptimizerComponent implements OnInit {

  counters: Counter[];
  selected: Counter[] = [];
  recommendations: { best: Counter[], good: Counter[], bad: Counter[] } = { best: [], good: [], bad: [] };

  constructor(private counterService: CounterService) {}

  ngOnInit() {
    this.counters = this.counterService.getCounters();
  }

  onClickType(counter: Counter) {
    if (this.isSelected(counter)) {
      const position = this.selected.findIndex((counterEl: Counter) => {
        return counterEl.id === counter.id;
      });
      this.selected.splice(position, 1);

    } else if (this.selected.length < 2) {
      this.selected.push(counter);
    }

    switch (this.selected.length) {
      case 0:
        this.recommendations = { best: [], good: [], bad: [] };
        break;
      case 1:
        this.recommendations = this.counterService.getAnalysis(this.selected[0]);
        break;
      case 2:
        this.recommendations = this.counterService.getAnalysis(this.selected[0], this.selected[1]);
        break;
    }
  }

  isSelected(counter: Counter) {
    return this.selected.indexOf(counter) >= 0;
  }

  isBest(counter: Counter) {
    return this.recommendations.best.indexOf(counter) >= 0;
  }

  isGood(counter: Counter) {
    return this.recommendations.good.indexOf(counter) >= 0;
  }

  isBad(counter: Counter) {
    return this.recommendations.bad.indexOf(counter) >= 0;
  }
}
