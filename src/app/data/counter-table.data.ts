export default [
  {
    id: 0,
    name: 'Bug',
    cname: '蟲',
    superEffective: [1, 9, 14],
    notVeryEffective: [4, 5, 6, 7, 8, 13, 16],
    noEffect: [],
    color: '#A7B81E',
    img: 'bug.png'
  }, {
    id: 1,
    name: 'Dark',
    cname: '惡',
    superEffective: [8, 14],
    notVeryEffective: [1, 4, 5],
    noEffect: [],
    color: '#705846',
    img: 'dark.png'
  }, {
    id: 2,
    name: 'Dragon',
    cname: '龍',
    superEffective: [2],
    notVeryEffective: [16],
    noEffect: [4],
    color: '#6C34F9',
    img: 'dragon.png'
  }, {
    id: 3,
    name: 'Electric',
    cname: '電擊',
    superEffective: [7, 17],
    notVeryEffective: [2, 3, 9],
    noEffect: [10],
    color: '#F5CE39',
    img: 'electric.png'
  }, {
    id: 4,
    name: 'Fairy',
    cname: '妖精',
    superEffective: [1, 2, 5],
    notVeryEffective: [6, 13, 16],
    noEffect: [],
    color: '#FB9AE1',
    img: 'fairy.png'
  }, {
    id: 5,
    name: 'Fighting',
    cname: '格鬥',
    superEffective: [1, 11, 12, 15, 16],
    notVeryEffective: [0, 4, 7, 13, 14],
    noEffect: [8],
    color: '#BB2D2E',
    img: 'fighting.png'
  }, {
    id: 6,
    name: 'Fire',
    cname: '火',
    superEffective: [0, 9, 11, 16],
    notVeryEffective: [2, 6, 15, 17],
    noEffect: [],
    color: '#EF8130',
    img: 'fire.png'
  }, {
    id: 7,
    name: 'Flying',
    cname: '飛行',
    superEffective: [0, 5, 9],
    notVeryEffective: [3, 15, 16],
    noEffect: [],
    color: '#A690EE',
    img: 'flying.png'
  }, {
    id: 8,
    name: 'Ghost',
    cname: '鬼',
    superEffective: [8, 14],
    notVeryEffective: [1],
    noEffect: [12],
    color: '#705A9A',
    img: 'ghost.png'
  }, {
    id: 9,
    name: 'Grass',
    cname: '草',
    superEffective: [10, 15, 17],
    notVeryEffective: [0, 2, 6, 7, 9, 13, 16],
    noEffect: [],
    color: '#75C04C',
    img: 'grass.png'
  }, {
    id: 10,
    name: 'Ground',
    cname: '地',
    superEffective: [3, 6, 13, 15, 16],
    notVeryEffective: [0, 9],
    noEffect: [7],
    color: '#D8C265',
    img: 'ground.png'
  }, {
    id: 11,
    name: 'Ice',
    cname: '冰',
    superEffective: [2, 7, 9, 10],
    notVeryEffective: [6, 11, 16, 17],
    noEffect: [],
    color: '#97D8D7',
    img: 'ice.png'
  }, {
    id: 12,
    name: 'Normal',
    cname: '一般',
    superEffective: [],
    notVeryEffective: [15, 16],
    noEffect: [8],
    color: '#AAAD7D',
    img: 'normal.png'
  }, {
    id: 13,
    name: 'Poison',
    cname: '毒',
    superEffective: [4, 9],
    notVeryEffective: [8, 10, 13, 15],
    noEffect: [16],
    color: '#9A3F9B',
    img: 'poison.png'
  }, {
    id: 14,
    name: 'Psychic',
    cname: '超能力',
    superEffective: [5, 13],
    notVeryEffective: [14, 16],
    noEffect: [1],
    color: '#F85689',
    img: 'psychic.png'
  }, {
    id: 15,
    name: 'Rock',
    cname: '岩',
    superEffective: [0, 6, 7, 11],
    notVeryEffective: [5, 10, 16],
    noEffect: [],
    color: '#B7A137',
    img: 'rock.png'
  }, {
    id: 16,
    name: 'Steel',
    cname: '鋼',
    superEffective: [4, 11, 15],
    notVeryEffective: [3, 6, 16, 17],
    noEffect: [],
    color: '#B5B3CA',
    img: 'steel.png'
  }, {
    id: 17,
    name: 'Water',
    cname: '水',
    superEffective: [6, 10, 15],
    notVeryEffective: [2, 9, 17],
    noEffect: [],
    color: '#6892EF',
    img: 'water.png'
  }
];
