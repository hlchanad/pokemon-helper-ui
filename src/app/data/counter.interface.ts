export interface Counter {
  id: number;
  name: string;
  cname: string;
  superEffective: number[];
  notVeryEffective: number[];
  noEffect: number[];
  color: string;
  img: string;
}
