import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AccordionModule } from 'ngx-bootstrap';

import { AppRoutingModule } from './app-routing.module';
import { HeaderComponent } from './header/header.component';
import { AppComponent } from './app.component';
import { CounterListComponent } from './counter-list/counter-list.component';
import { CounterService } from './shared/counter.service';
import { AttackOptimizerComponent } from './attack-optimizer/attack-optimizer.component';
import { AccordionBackgroundColorDirective } from './shared/accordion-background-color.directive';
import { SquareDirective } from './shared/square.directive';

@NgModule({
  declarations: [
    AppComponent,
    CounterListComponent,
    AccordionBackgroundColorDirective,
    HeaderComponent,
    AttackOptimizerComponent,
    SquareDirective
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AppRoutingModule,
    AccordionModule.forRoot()
  ],
  providers: [CounterService],
  bootstrap: [AppComponent]
})
export class AppModule { }
