import { Counter } from '../data/counter.interface';
import ct from '../data/counter-table.data';

export class CounterService {
  counters: Counter[] = ct;

  public getCounters() {
    return this.counters.slice();
  }

  public getCounter(id: number): Counter {
    const counter = this.counters.find((counterEl: Counter) => {
      return counterEl.id === id;
    });
    return (counter !== undefined) ? counter : null;
  }

  public getAnalysis(defenser1: Counter, defenser2?: Counter) {

    const priority = this.getPriority(defenser1, defenser2);

    priority.sort((a: {id: number, score: number}, b: {id: number, score: number}) => {
      return b.score - a.score;
    });


    const bestOutput: Counter[] = [];
    const best = priority.filter((p: {id: number, score: number}) => {
      return p.score > 1;
    });
    for (let i = 0; i < best.length; i++) {
      bestOutput.push(this.getCounter(best[i].id));
    }

    const goodOutput: Counter[] = [];
    const good = priority.filter((p: {id: number, score: number}) => {
      return p.score === 1;
    });
    for (let i = 0; i < good.length; i++) {
      goodOutput.push(this.getCounter(good[i].id));
    }

    const badOutput: Counter[] = [];
    const bad = priority.filter((p: {id: number, score: number}) => {
      return p.score < 0;
    });
    for (let i = 0; i < bad.length; i++) {
      badOutput.push(this.getCounter(bad[i].id));
    }

    return { best: bestOutput, good: goodOutput, bad: badOutput};
  }

  private getPriority(defenser1: Counter, defenser2?: Counter) {

    // index: id, value: score
    const priority: {id: number, score: number}[] = []; // assumed index k item always has id k

    // presetting score of 0 for all type
    for (let i = 0; i < this.counters.length; i++) {
      priority[this.counters[i].id] = { id: this.counters[i].id, score: 0 };
    }

    for (let i = 0; i < this.counters.length; i++) {
      const attacker = this.counters[i];

      if (attacker.superEffective.indexOf(defenser1.id) >= 0) {
        priority[attacker.id].score ++;
      } else if (attacker.notVeryEffective.indexOf(defenser1.id) >= 0) {
        priority[attacker.id].score --;
      } else if (attacker.noEffect.indexOf(defenser1.id) >= 0) {
        priority[attacker.id].score -= 2;
      }

      if (defenser2 === undefined) { continue; }

      if (attacker.superEffective.indexOf(defenser2.id) >= 0) {
        priority[attacker.id].score ++;
      } else if (attacker.notVeryEffective.indexOf(defenser2.id) >= 0) {
        priority[attacker.id].score --;
      } else if (attacker.noEffect.indexOf(defenser2.id) >= 0) {
        priority[attacker.id].score -= 2;
      }
    }

    return priority;
  }
}
