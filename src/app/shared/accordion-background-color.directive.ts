import { Directive, ElementRef, Input, OnInit, Renderer2 } from '@angular/core';

@Directive({
  selector: '[appAccordionBackgroundColor]'
})
export class AccordionBackgroundColorDirective implements OnInit {

  @Input() bgColor = 'transparent';

  constructor(private elementRef: ElementRef, private renderer: Renderer2) { }

  ngOnInit() {
    // set bg color for: .card
    const cardElement = this.elementRef.nativeElement;
    this.renderer.setStyle(cardElement, 'backgroundColor', this.bgColor);

    // set bg color for: .card .card-header
    const cardHeaderElement = this.elementRef.nativeElement.querySelector('.card-header');
    this.renderer.setStyle(cardHeaderElement, 'backgroundColor', this.bgColor);
    this.renderer.setStyle(cardHeaderElement, 'color', 'white');

    // set bg color for: .panel
    const panelElement = this.elementRef.nativeElement.querySelector('.panel');
    this.renderer.setStyle(panelElement, 'backgroundColor', this.bgColor);
    this.renderer.setStyle(panelElement, 'border', this.bgColor);

    // set bg color for: .panel-body
    const panelBodyElement = this.elementRef.nativeElement.querySelector('.panel-body');
    this.renderer.setStyle(panelBodyElement, 'backgroundColor', 'rgba(255, 255, 255, 0.9)');
  }
}
