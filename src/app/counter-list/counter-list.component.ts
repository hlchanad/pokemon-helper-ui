import { Component, OnInit } from '@angular/core';

import { CounterService } from '../shared/counter.service';
import { Counter } from '../data/counter.interface';

@Component({
  selector: 'app-counter-list',
  templateUrl: './counter-list.component.html',
  styleUrls: ['./counter-list.component.css'],
})
export class CounterListComponent implements OnInit {

  counters: Counter[];

  constructor(private counterService: CounterService) { }

  ngOnInit() {
    this.counters = this.counterService.getCounters();
  }

  getCounter(id: number) {
    return this.counterService.getCounter(id);
  }
}
