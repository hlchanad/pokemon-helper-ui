import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CounterListComponent } from './counter-list/counter-list.component';
import { AttackOptimizerComponent } from './attack-optimizer/attack-optimizer.component';

const appRoutes: Routes = [
  { path: '', redirectTo: '/counter-list', pathMatch: 'full' },
  { path: 'counter-list', component: CounterListComponent },
  { path: 'attack-optimizer', component: AttackOptimizerComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
